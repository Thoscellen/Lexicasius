﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Xml.Serialization;

namespace WordGen.Model {

	/// <summary>
	/// Représente un gestionnaire de sauvegarde et de lecture d'un ou plusieurs Syllabaires pour l'application en exposant des méthodes permettant de gérer des fichiers XML représentant des Syllabaires.
	/// </summary>
	public class SyllabaryIoManager : FileIoManager {

		/// <summary>
		/// Représente le dossier local applicatif et utilisateur.
		/// </summary>
		public static readonly String UserAppData = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), Assembly.GetEntryAssembly().GetName().Name);


		/// <summary>
		/// Sauvegarde le Syllabaire dans un fichier a l'emplacement indiqué. Utilise le titre comme nom de fichier.
		/// </summary>
		/// <param name="thisSyllabary">Le sillabraire à sauvegarder dans un fichier XML</param>
		/// <param name="thisPath">Le chemin d'accès au fichier dans lequel le Syllabaire sera enregistré.</param>
		public void saveSyllabary(Syllabary thisSyllabary, string thisPath) {
			thisPath = Path.Combine(thisPath, thisSyllabary.title);
			thisPath = Path.ChangeExtension(thisPath, ".xml");
			XmlSerializer xs = new XmlSerializer(typeof(Syllabary));
			using (StreamWriter wr = new StreamWriter(thisPath)) {
				xs.Serialize(wr, thisSyllabary);
			}
		}

		/// <summary>
		/// Charge le Syllabaire depuis le fichier précisé
		/// </summary>
		/// <param name="thisSillabaryFile">L'emplacement du fichier XML représentant un Syllabaire</param>
		public Syllabary loadSyllabary(string thisSillabaryFile) {
			XmlSerializer xs = new XmlSerializer(typeof(Syllabary));
			using (StreamReader sr = new StreamReader(thisSillabaryFile)) {
				Syllabary mySylla = xs.Deserialize(sr) as Syllabary;
				return mySylla;
			}
		}

		/// <summary>
		/// Charge l'ensemble des fichier Syllabaire depuis le dossier précisé
		/// </summary>
		/// <param name="thisFolder">L'emplacement du dossier dans lequel les Syllabaires seront chargés.</param>
		public List<Syllabary> loadAllSyllabaries(string thisFolder) {
			List<Syllabary> openedSylla = new List<Syllabary>();
			foreach (string aFile in Directory.GetFiles(thisFolder)) {
				if (Path.GetExtension(aFile).ToLower().Equals(".xml")) {
					try {
						openedSylla.Add(this.loadSyllabary(aFile));
					} catch (InvalidOperationException ) {
						//Some XML files are not Syllables.
					}
				}
			}
			return openedSylla;

		}

		/// <summary>
		/// Sauvegarde l'ensemble des objets Syllabaires spécifiés à l'emplacement spécifié.
		/// </summary>
		/// <param name="thosesSillabaries">La liste des Syllabaires à sauvegarder dans des fichiers XML.</param>
		/// <param name="thisFolder">L'emplacement du dossier dans lequel les Syllabaires seront créés.</param>
		public void saveAllSyllabaries(List<Syllabary> thosesSillabaries, string thisFolder) {
			foreach (Syllabary aSylla in thosesSillabaries) {
				saveSyllabary(aSylla, thisFolder);
			}
		}
	}
}