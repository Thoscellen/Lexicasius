﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WordGen.Model.Exceptions;
using WordGen.View.LocalizationString;

namespace WordGen.View {
	public partial class ResultForm : Form {

		private bool _isLocked;
		public bool isLocked
		{
			get { return _isLocked; }
			set
			{
				_isLocked = value;
				BlockForms.Checked = value;
				BlockForms.Image = (value) ? WordGen.Properties.Resources.lock_16xLG : WordGen.Properties.Resources.unlock_16xLG;
			}
		}


		public ResultForm() {
			InitializeComponent();
		}

		private void ResultForm_Load(object sender, EventArgs e) {
			BviewSmall_Click(sender, e);
			isLocked = true;
		}

		private void ResultForm_Move(object sender, EventArgs e) {
			if (this.ContainsFocus) {
				isLocked = false;
			}
		}

		private void BLockMove_Click(object sender, EventArgs e) {
			isLocked = !BlockForms.Checked;
		}

		internal void setList(Dictionary<string, GeneratorException> results) {
			LVresults.Items.Clear();
			foreach (KeyValuePair<string, GeneratorException> aWord in results) {
				ListViewItem aLvi = new ListViewItem();
				aLvi.Text = aWord.Key;
				aLvi.ImageIndex = (aWord.Value != null) ? 1 : 0;
				if (aWord.Value != null) {
					ListViewItem.ListViewSubItem aLvsi = new ListViewItem.ListViewSubItem(aLvi, aWord.Value.Message);
					if (aWord.Value.GetType() == new EmptyConsonnantListException().GetType()) {
						aLvi.Tag = (EmptyConsonnantListException)aWord.Value;
					} else if (aWord.Value.GetType() == new EmptyVowelListException().GetType()) {
						aLvi.Tag = (EmptyVowelListException)aWord.Value;
					} else if (aWord.Value.GetType() == new EmptySyllableListException().GetType()) {
						aLvi.Tag = (EmptySyllableListException)aWord.Value;
					} else if (aWord.Value.GetType() == new NoBeforeSyllableMatchException().GetType()) {
						aLvi.Tag = (NoBeforeSyllableMatchException)aWord.Value;
					} else {
						aLvi.Tag = aWord.Value;
					}
				}
				LVresults.Items.Add(aLvi);
			}
		}

		internal void showWordInfo() {
			GeneratorException reason = (GeneratorException)LVresults.SelectedItems[0].Tag;
			MessageBox.Show(reason.Message, Messages.ResultForm_InformationTitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void BviewDetail_Click(object sender, EventArgs e) {
			LVresults.View = System.Windows.Forms.View.Details;
			Bview.Image = BviewDetail.Image;
			LVresults.Columns[0].Width = Convert.ToInt32(0.3 * this.Width);
			LVresults.Columns[1].Width = Convert.ToInt32(0.7 * this.Width) - 18;
			BviewDetail.Checked = true;
			BviewSmall.Checked = false;
			BviewTile.Checked = false;
		}

		private void BviewSmall_Click(object sender, EventArgs e) {
			LVresults.View = System.Windows.Forms.View.SmallIcon;
			Bview.Image = BviewSmall.Image;
			BviewDetail.Checked = false;
			BviewSmall.Checked = true;
			BviewTile.Checked = false;
		}

		private void BviewTile_Click(object sender, EventArgs e) {
			LVresults.View = System.Windows.Forms.View.Tile;
			Bview.Image = BviewTile.Image;
			BviewDetail.Checked = false;
			BviewSmall.Checked = false;
			BviewTile.Checked = true;
		}

		private void ResultForm_FormClosed(object sender, FormClosedEventArgs e) {
			this.Dispose();
		}

		private void ResultForm_Resize(object sender, EventArgs e) {
			if (LVresults.View == System.Windows.Forms.View.Details) {
				LVresults.Columns[0].Width = Convert.ToInt32(0.3 * this.Width);
				LVresults.Columns[1].Width = Convert.ToInt32(0.7 * this.Width) - 18;
			} else {
				LVresults.ArrangeIcons(ListViewAlignment.SnapToGrid);
			}
		}

		private void Binfo_Click(object sender, EventArgs e) {
			if (LVresults.SelectedItems[0].Tag != null) {
				showWordInfo();
			}
		}

		private void LVresults_SelectedIndexChanged(object sender, EventArgs e) {
			var hasSelection = (LVresults.SelectedItems.Count > 0);
			Bcopy.Enabled = hasSelection;
			Binfo.Enabled = (hasSelection && (LVresults.SelectedItems[0].Tag != null));
		}

		private void LVresults_DoubleClick(object sender, EventArgs e) {
			if (LVresults.SelectedItems[0].Tag != null) {
				showWordInfo();
			}
		}

		private void ResultForm_Shown(object sender, EventArgs e) {
		}

	}
}
