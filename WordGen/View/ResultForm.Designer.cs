﻿namespace WordGen.View {
	partial class ResultForm {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ResultForm));
			System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem(new string[] {
            "Mot incomplet",
            resources.GetString("LVresults.Items")}, 1);
			System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem("Mot", 0);
			this.TSresult = new System.Windows.Forms.ToolStrip();
			this.Bcopy = new System.Windows.Forms.ToolStripButton();
			this.Binfo = new System.Windows.Forms.ToolStripButton();
			this.Sep1 = new System.Windows.Forms.ToolStripSeparator();
			this.Bsave = new System.Windows.Forms.ToolStripButton();
			this.BlockForms = new System.Windows.Forms.ToolStripButton();
			this.Bview = new System.Windows.Forms.ToolStripDropDownButton();
			this.BviewDetail = new System.Windows.Forms.ToolStripMenuItem();
			this.BviewSmall = new System.Windows.Forms.ToolStripMenuItem();
			this.BviewTile = new System.Windows.Forms.ToolStripMenuItem();
			this.LVresults = new System.Windows.Forms.ListView();
			this.CHname = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.CHerror = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.ILresultsLarge = new System.Windows.Forms.ImageList(this.components);
			this.ILresultsSmall = new System.Windows.Forms.ImageList(this.components);
			this.ttErrorInfo = new System.Windows.Forms.ToolTip(this.components);
			this.TSresult.SuspendLayout();
			this.SuspendLayout();
			// 
			// TSresult
			// 
			this.TSresult.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
			this.TSresult.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Bcopy,
            this.Binfo,
            this.Sep1,
            this.Bsave,
            this.BlockForms,
            this.Bview});
			this.TSresult.Location = new System.Drawing.Point(0, 0);
			this.TSresult.Name = "TSresult";
			this.TSresult.Size = new System.Drawing.Size(275, 25);
			this.TSresult.TabIndex = 0;
			this.TSresult.Text = "toolStrip1";
			// 
			// Bcopy
			// 
			this.Bcopy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.Bcopy.Image = global::WordGen.Properties.Resources.Copy_6524;
			this.Bcopy.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.Bcopy.Name = "Bcopy";
			this.Bcopy.Size = new System.Drawing.Size(23, 22);
			this.Bcopy.Text = "BCopy";
			// 
			// Binfo
			// 
			this.Binfo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.Binfo.Image = global::WordGen.Properties.Resources.StatusAnnotations_Information_16xMD;
			this.Binfo.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.Binfo.Name = "Binfo";
			this.Binfo.Size = new System.Drawing.Size(23, 22);
			this.Binfo.Text = "Binfo";
			this.Binfo.Click += new System.EventHandler(this.Binfo_Click);
			// 
			// Sep1
			// 
			this.Sep1.Name = "Sep1";
			this.Sep1.Size = new System.Drawing.Size(6, 25);
			// 
			// Bsave
			// 
			this.Bsave.Image = global::WordGen.Properties.Resources.save_16xLG;
			this.Bsave.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.Bsave.Name = "Bsave";
			this.Bsave.Size = new System.Drawing.Size(92, 22);
			this.Bsave.Text = "Enregistrer...";
			// 
			// BlockForms
			// 
			this.BlockForms.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
			this.BlockForms.Checked = true;
			this.BlockForms.CheckState = System.Windows.Forms.CheckState.Checked;
			this.BlockForms.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.BlockForms.Image = global::WordGen.Properties.Resources.lock_16xLG;
			this.BlockForms.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.BlockForms.Name = "BlockForms";
			this.BlockForms.Size = new System.Drawing.Size(23, 22);
			this.BlockForms.Text = "Lier les deux fenêtres";
			this.BlockForms.Click += new System.EventHandler(this.BLockMove_Click);
			// 
			// Bview
			// 
			this.Bview.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
			this.Bview.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.Bview.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BviewDetail,
            this.BviewSmall,
            this.BviewTile});
			this.Bview.Image = ((System.Drawing.Image)(resources.GetObject("Bview.Image")));
			this.Bview.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.Bview.Name = "Bview";
			this.Bview.Size = new System.Drawing.Size(29, 22);
			this.Bview.Text = "Affichage des mots";
			this.Bview.ToolTipText = "Affichage des mots";
			// 
			// BviewDetail
			// 
			this.BviewDetail.Image = global::WordGen.Properties.Resources.Views_Detail_16;
			this.BviewDetail.Name = "BviewDetail";
			this.BviewDetail.Size = new System.Drawing.Size(120, 22);
			this.BviewDetail.Text = "Détail";
			this.BviewDetail.Click += new System.EventHandler(this.BviewDetail_Click);
			// 
			// BviewSmall
			// 
			this.BviewSmall.Image = global::WordGen.Properties.Resources.Views_list_16;
			this.BviewSmall.Name = "BviewSmall";
			this.BviewSmall.Size = new System.Drawing.Size(120, 22);
			this.BviewSmall.Text = "Liste";
			this.BviewSmall.Click += new System.EventHandler(this.BviewSmall_Click);
			// 
			// BviewTile
			// 
			this.BviewTile.Image = global::WordGen.Properties.Resources.Views_Content_16;
			this.BviewTile.Name = "BviewTile";
			this.BviewTile.Size = new System.Drawing.Size(120, 22);
			this.BviewTile.Text = "Contenu";
			this.BviewTile.Click += new System.EventHandler(this.BviewTile_Click);
			// 
			// LVresults
			// 
			this.LVresults.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.LVresults.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.CHname,
            this.CHerror});
			this.LVresults.Dock = System.Windows.Forms.DockStyle.Fill;
			this.LVresults.FullRowSelect = true;
			this.LVresults.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
			this.LVresults.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem3,
            listViewItem4});
			this.LVresults.LargeImageList = this.ILresultsLarge;
			this.LVresults.Location = new System.Drawing.Point(0, 25);
			this.LVresults.Name = "LVresults";
			this.LVresults.ShowGroups = false;
			this.LVresults.ShowItemToolTips = true;
			this.LVresults.Size = new System.Drawing.Size(275, 436);
			this.LVresults.SmallImageList = this.ILresultsSmall;
			this.LVresults.TabIndex = 1;
			this.LVresults.UseCompatibleStateImageBehavior = false;
			this.LVresults.View = System.Windows.Forms.View.SmallIcon;
			this.LVresults.SelectedIndexChanged += new System.EventHandler(this.LVresults_SelectedIndexChanged);
			this.LVresults.DoubleClick += new System.EventHandler(this.LVresults_DoubleClick);
			// 
			// CHname
			// 
			this.CHname.Text = "Mot";
			this.CHname.Width = 90;
			// 
			// CHerror
			// 
			this.CHerror.Text = "Erreur";
			this.CHerror.Width = 182;
			// 
			// ILresultsLarge
			// 
			this.ILresultsLarge.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ILresultsLarge.ImageStream")));
			this.ILresultsLarge.TransparentColor = System.Drawing.Color.Transparent;
			this.ILresultsLarge.Images.SetKeyName(0, "Textfile_818_32x.png");
			this.ILresultsLarge.Images.SetKeyName(1, "BrokenlinktoFile_431_32x.png");
			// 
			// ILresultsSmall
			// 
			this.ILresultsSmall.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ILresultsSmall.ImageStream")));
			this.ILresultsSmall.TransparentColor = System.Drawing.Color.Transparent;
			this.ILresultsSmall.Images.SetKeyName(0, "Textfile_818_16x.png");
			this.ILresultsSmall.Images.SetKeyName(1, "BrokenlinktoFile_431_16x.png");
			this.ILresultsSmall.Images.SetKeyName(2, "Strings_7959_16x.png");
			// 
			// ResultForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(275, 461);
			this.Controls.Add(this.LVresults);
			this.Controls.Add(this.TSresult);
			this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimumSize = new System.Drawing.Size(250, 320);
			this.Name = "ResultForm";
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "ResultForm";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ResultForm_FormClosed);
			this.Load += new System.EventHandler(this.ResultForm_Load);
			this.Shown += new System.EventHandler(this.ResultForm_Shown);
			this.Move += new System.EventHandler(this.ResultForm_Move);
			this.Resize += new System.EventHandler(this.ResultForm_Resize);
			this.TSresult.ResumeLayout(false);
			this.TSresult.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ToolStrip TSresult;
		private System.Windows.Forms.ToolStripButton Bcopy;
		private System.Windows.Forms.ToolStripSeparator Sep1;
		private System.Windows.Forms.ToolStripButton Bsave;
		private System.Windows.Forms.ToolStripButton BlockForms;
		private System.Windows.Forms.ListView LVresults;
		private System.Windows.Forms.ColumnHeader CHname;
		private System.Windows.Forms.ColumnHeader CHerror;
		private System.Windows.Forms.ToolStripDropDownButton Bview;
		private System.Windows.Forms.ToolStripMenuItem BviewDetail;
		private System.Windows.Forms.ToolStripMenuItem BviewSmall;
		private System.Windows.Forms.ToolStripMenuItem BviewTile;
		private System.Windows.Forms.ImageList ILresultsSmall;
		private System.Windows.Forms.ImageList ILresultsLarge;
		private System.Windows.Forms.ToolStripButton Binfo;
		private System.Windows.Forms.ToolTip ttErrorInfo;
	}
}