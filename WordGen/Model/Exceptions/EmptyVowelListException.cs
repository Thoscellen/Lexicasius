﻿using System;
using System.Runtime.Serialization;
using WordGen.Model.Exceptions;
using WordGen.View.LocalizationString;

namespace WordGen.Model.Exceptions {
	/// <summary>
	/// Aucune voyelle n'as été trouvée. Ajoutez des voyelles à votre syllabaire, retirez les voyelles de la syntaxe de votre mot (caractère 'v') ou paramétrez votre syllabaire pour ne pas insérer automatiquement une voyelle entre une consonne et une syllabe.
	/// </summary>
	[Serializable]
	internal class EmptyVowelListException : GeneratorException {
		public EmptyVowelListException() : base(Messages.GeneratorError_EmptyVowelListException) {
		}

		public EmptyVowelListException(string wordSoFar, string currentSyllable, string currentSyntaxe, Syllabary currentSyllabary) : base(wordSoFar, currentSyllable, currentSyntaxe, currentSyllabary, Messages.GeneratorError_EmptyVowelListException) {
		}

		public EmptyVowelListException(string wordSoFar, string currentSyllable, string currentSyntaxe, Syllabary currentSyllabary, string message) : base(wordSoFar, currentSyllable, currentSyntaxe, currentSyllabary, message) {
		}

		protected EmptyVowelListException(string wordSoFar, string currentSyllable, string currentSyntaxe, Syllabary currentSyllabary, string message, Exception innerException) : base(wordSoFar, currentSyllable, currentSyntaxe, currentSyllabary, message, innerException) {
		}
	}
}