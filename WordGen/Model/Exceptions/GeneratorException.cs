﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using WordGen.View.LocalizationString;

namespace WordGen.Model.Exceptions {
	/// <summary>
	/// Une exception n'as pas permis de générer entièrement le mot.
	/// </summary>
	class GeneratorException : Exception {

		/// <summary>
		/// Le mot généré auparavant.
		/// </summary>
		protected string _wordSoFar;
		/// <summary>
		/// Obtient le mot généré auparavant.
		/// </summary>
		public string wordSoFar
		{
			get { return _wordSoFar; }
		}

		/// <summary>
		/// Le caractère (consonne, voyelle, syllabe) qui allait être ajouté.
		/// </summary>
		protected string _currentSyllable;
		/// <summary>
		/// Obtient le caractère (consonne, voyelle, syllabe) qui allait être ajouté.
		/// </summary>
		public string currentSyllable
		{
			get { return _currentSyllable; }
		}

		/// <summary>
		/// La syntaxe utilisée pour générer le mot.
		/// </summary>
		protected string _currentSyntaxe;
		/// <summary>
		/// Obtient la syntaxe utilisée pour générer le mot.
		/// </summary>
		public string currentSyntaxe
		{
			get { return _currentSyntaxe; }
		}

		/// <summary>
		/// Le syllabaire utilisé pour générer le mot.
		/// </summary>
		protected Syllabary _currentSyllabary;
		/// <summary>
		/// Obtient le syllabaire utilisé pour générer le mot.
		/// </summary>
		public Syllabary currentSyllabary
		{
			get { return _currentSyllabary; }
		}

		public GeneratorException() : base(Messages.GeneratorError_GenericGeneratorException) {
		}

		public GeneratorException(string message) : base(message) {
		}

		public GeneratorException(string wordSoFar, string currentSyllable, string currentSyntaxe, Syllabary currentSyllabary) : base(Messages.GeneratorError_GenericGeneratorException) {
			init(wordSoFar, currentSyllable, currentSyntaxe, currentSyllabary);
		}

		public GeneratorException(string wordSoFar, string currentSyllable, string currentSyntaxe, Syllabary currentSyllabary, string message) : base(message) {
			init(wordSoFar, currentSyllable, currentSyntaxe, currentSyllabary);
		}

		public GeneratorException(string wordSoFar, string currentSyllable, string currentSyntaxe, Syllabary currentSyllabary, string message, Exception innerException) : base(message, innerException) {
			init(wordSoFar, currentSyllable, currentSyntaxe, currentSyllabary);
		}

		/// <summary>
		/// Associe les éléments passé en paramètre avec les champs de manière factorisée.
		/// </summary>
		protected void init(string wordSoFar, string currentSyllable, string currentSyntaxe, Syllabary currentSyllabary) {
			this._wordSoFar = wordSoFar;
			this._currentSyllable = currentSyllable;
			this._currentSyntaxe = currentSyntaxe;
			this._currentSyllabary = currentSyllabary;
		}

	}
}
