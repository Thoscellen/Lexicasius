﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

	/// <summary>
	/// Represends a manageable collection of syllables, consonants and/or vowels.
	/// </summary>
	[XmlRoot]
	public class Syllabary : ICloneable {
		/// <summary>
		/// The title of the syllabary, like a language.
		/// </summary>
		[XmlElement("Title")]
		public string title { get; set; }
		/// <summary>
		/// The list of syllable the syllabary contains. A syllable is a combination of consonants and vowels.
		/// </summary>
		[XmlArray("Syllables")]
		[XmlArrayItem("Syllable")]
		public List<string> syllables { get; set; }
		/// <summary>
		/// The list of consonants the syllabary contains. A consonant is a speech sound that is articulated with complete or partial closure of the vocal tract.
		/// </summary>
		[XmlArray("Consonants")]
		[XmlArrayItem("consonant")]
		public List<string> consonants { get; set; }
		/// <summary>
		/// The list of vowels or semivowels the syllabary contains. A vowel is a sound pronounced with an open vocal tract so that there is no build-up of air pressure at any point above the glottis.
		/// </summary>
		[XmlArray("Vowels")]
		[XmlArrayItem("Vowel")]
		public List<string> vowels { get; set; }
		/// <summary>
		/// The way the software will join a syllable with the previous added element.
		/// </summary>
		[XmlElement("beforeSyllable")]
		public BeforeSyllable beforeSyllable;
		/// <summary>
		/// The way the software will join the next element with the added syllable.
		/// </summary>
		[XmlElement("afterSyllable")]
		public AfterSyllable afterSyllable;

		public Syllabary() {
			this.syllables = new List<string>();
			this.consonants = new List<string>();
			this.vowels = new List<string>();
		}

		protected Syllabary(Syllabary aSyllabary) {
			title = aSyllabary.title;
			syllables = new List<string>(aSyllabary.syllables);
			consonants = new List<string>(aSyllabary.consonants);
			vowels = new List<string>(aSyllabary.vowels);
			beforeSyllable = aSyllabary.beforeSyllable;
			afterSyllable = aSyllabary.afterSyllable;
    }

		public object Clone() {
			return new Syllabary(this);
		}
	}
